#!/usr/bin/env bash

# FIXME fuck this file is a fucking spaghettified piece of shit.
# FIXME Maybe this should have just used mtools.

export M310_BASE_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

# The Debian version is required because the "legacy" tgz files are not compiled
# with SSL support.
MONGODB_NAME='mongodb-linux-x86_64-debian81-3.4.1'
MONGODB_DIR="${M310_BASE_DIR}/${MONGODB_NAME}"
MONGODB_BIN_DIR="${MONGODB_DIR}/bin"
export PATH="${MONGODB_BIN_DIR}:${PATH}"

export DB_PATH=./dbpath
mkdir -pv "${DB_PATH}"

# If replica-set operations fail then it could be because this timeout is too
# short.  Consider decreasing it locally only when you know the homework will
# still validate.
export HW_SCRIPT_TIMEOUT=30s

function cdhw() {
    WEEK=$1
    HOMEWORK=$2
    cd ${M310_BASE_DIR}/week-${WEEK}-*/hw-${HOMEWORK}-*
}
export -f cdhw

function cdfe() {
    QUESTION=$1
    cd ${M310_BASE_DIR}/week-3-final-exam/question-${QUESTION}
}
export -f cdfe

function m310_install_mongodb_enterprise() {
    sudo apt-get install libcurl3 snmp
    pushd ${M310_BASE_DIR}/mongodb-enterprise
    DEB_FILES=$(ls)
    for DEB in ${DEB_FILES} ; do
        echo
        echo "Attempting to install: ${DEB}"
        sudo dpkg -i "$(pwd)/${DEB}"
        sudo apt-get install -f
    done
    popd
}
export -f m310_install_mongodb_enterprise

function m310_setup_project() {
    pushd ${M310_BASE_DIR}

#    rm -rf "${MONGODB_NAME}"
#    tar -xvf "${MONGODB_NAME}.tgz"
    m310_install_mongodb_enterprise

    sudo apt-get install virtualbox
    sudo dpkg --install ./vagrant_1.9.1_x86_64.deb
    vagrant plugin install vagrant-vbguest

#    rm -rf mtools
#    git clone https://github.com/rueckstiess/mtools.git

    popd
}
export -f m310_setup_project

HEAD_COUNT=0
function m310_print_header() {
    HEAD_COUNT=$((HEAD_COUNT + 1))
    echo
    echo '========================================'
    echo "(${HEAD_COUNT}) ${@}"
}
export -f m310_print_header

#function assertequals() {
#    EXPECTED=$1
#    ACTUAL=$2
#    MESSAGE=$3
#    if [ "x${EXPECTED}" != "x${ACTUAL}" ] ; then
#        echo -n "ERROR: Expected '${EXPECTED}' does not equal actual '${ACTUAL}'"
#        [ -n "${MESSAGE}" ] && echo -n ": ${MESSAGE}"
#        echo '.'
#        false
#        exit
#    fi
#}
#export -f assertequals

function m310_start_single_mongod() {
    PORT=$1

    [ -z "${HW}" ] && echo 'Variable not set: HW' && exit

    [ -z "${DB_REPL_SET_NAME}" ] && export DB_REPL_SET_NAME="M310_HW${HW}_REPL_SET"

    # This is used so we don't modify MONGOD_ARGS outside of this method.
    # Problems arise from multiple invocations because the keyFile argument is
    # added multiple times.
    THIS_MONGOD_ARGS="
        ${MONGOD_ARGS}
        --replSet=${DB_REPL_SET_NAME}
        "

    [ ! -z "${DB_KEYFILE}" ] && THIS_MONGOD_ARGS="${THIS_MONGOD_ARGS} --keyFile=${DB_KEYFILE}"
    [ ! -z "${DB_ENCRYPTION_KEY}" ] && THIS_MONGOD_ARGS="${THIS_MONGOD_ARGS}
        --enableEncryption
        --encryptionKeyFile=${DB_ENCRYPTION_KEY}"

    if [ ! -z "${MONGOD_AUDITING_ARGS}" ] ; then
        MONGOD_AUDITING_FILE="${DB_PATH}/r${PORT: -1}/auditLog.json"
        mkdir -pv $(dirname ${MONGOD_AUDITING_FILE})
        touch ${MONGOD_AUDITING_FILE}
        THIS_MONGOD_ARGS="${THIS_MONGOD_ARGS}
            ${MONGOD_AUDITING_ARGS}
            --auditPath=${MONGOD_AUDITING_FILE}
            "
    fi

    [ ! -z "${MONGOD_AUDIT_FILTERS}" ] && THIS_MONGOD_ARGS="${THIS_MONGOD_ARGS} ${MONGOD_AUDIT_FILTERS}"

    mkdir -pv "${DB_PATH}/r${PORT}"
    MONGOD_CMD="mongod
        --fork
        --dbpath=${DB_PATH}/r${PORT}
        --logpath=${DB_PATH}/r${PORT}/mongod-${PORT}.log
        --port="${PORT}"
        ${THIS_MONGOD_ARGS}"
    MONGOD_CMD="$(echo ${MONGOD_CMD} | sed -r 's/\s+/ /g')"
    echo "Starting: ${MONGOD_CMD}"
    eval ${MONGOD_CMD}
}
export -f m310_start_single_mongod

function m310_start_local_rs() {

    declare -a DB_PORTS=( "$@" )
    for port in ${DB_PORTS[@]} ; do
        echo
        m310_start_single_mongod ${port}
    done

    echo
    echo "Sleeping for ${HW_SCRIPT_TIMEOUT} after starting replica-set."
    sleep ${HW_SCRIPT_TIMEOUT}
}
export -f m310_start_local_rs

function m310_stop_local_rs() {
    declare -a DB_PORTS=( "$@" )
    for port in ${DB_PORTS[@]} ; do
        mongod --shutdown --dbpath="${DB_PATH}/r${port}"
    done
    killall mongod
}
export -f m310_stop_local_rs

function m310_destroy_local_rs() {
    declare -a DB_PORTS=( "$@" )
    m310_stop_local_rs ${DB_PORTS[@]}
    rm -rf "${DB_PATH}"
}
export -f m310_destroy_local_rs

function m310_initiate_rs() {
    [ -z "${DB_REPL_SET_NAME}" ] && echo 'Variable not set: DB_REPL_SET_NAME' && exit

    declare -a DB_PORTS=( "$@" )
    DB_HOST=localhost

    DB_SCRIPT="rs.initiate({_id: '${DB_REPL_SET_NAME}', members: ["
    for ((i=0; i < ${#DB_PORTS[@]}; i++)) ; do
        PORT=${DB_PORTS[${i}]}
        DB_SCRIPT="${DB_SCRIPT} { _id: ${i}, host: '${DB_HOST}:${PORT}' }, "
    done
    DB_SCRIPT="${DB_SCRIPT} ]});"

    echo "Initiating replica-set: ${DB_SCRIPT}"
    echo "With MONGO_ARGS: ${MONGO_ARGS}"
    MONGO_CMD="$(echo ${MONGO_CMD} | sed -r 's/\s+/ /g')"
    mongo ${MONGO_ARGS} --host="${DB_HOST}" --port=${DB_PORTS[0]} --eval "${DB_SCRIPT}"

    echo
    echo "Sleeping for ${HW_SCRIPT_TIMEOUT} after initiating replica-set."
    sleep ${HW_SCRIPT_TIMEOUT}
}
export -f m310_initiate_rs

function m310_determine_primary_node() {
    declare -a DB_PORTS=( "$@" )
    MONGO_CMD="$(echo ${MONGO_CMD} | sed -r 's/\s+/ /g')"
    DB_HOST=localhost
    for port in ${DB_PORTS[@]} ; do
        is_master=$(mongo ${MONGO_ARGS} \
                    --host=${DB_HOST} \
                    --port ${port} \
                    --quiet \
                    --eval "rs.isMaster().ismaster")
        if [[ "x${is_master}" == "xtrue" ]] ; then
            export DB_PRIMARY_PORT=${port}
            echo "Primary node is: ${DB_PRIMARY_PORT}"
            break
        fi
    done
    [ -z "${DB_PRIMARY_PORT}" ] && echo 'ERROR: Cannot determine master node.' 2>&1 && exit
}
export -f m310_determine_primary_node

function m310_print_secondary_nodes() {
    declare -a DB_PORTS=( "$@" )
    declare -a DB_SECONDARY_PORTS=( )
    MONGO_CMD="$(echo ${MONGO_CMD} | sed -r 's/\s+/ /g')"
    DB_HOST=localhost
    for port in ${DB_PORTS[@]} ; do
        is_master=$(mongo ${MONGO_ARGS} \
                    --host=${DB_HOST} \
                    --port ${port} \
                    --quiet \
                    --eval "rs.isMaster().ismaster")
        if [[ "x${is_master}" == "xfalse" ]] ; then
            DB_SECONDARY_PORTS+=( "${port}" )
        fi
    done
    echo "${DB_SECONDARY_PORTS[@]}"
}
export -f m310_print_secondary_nodes

function m310_create_keyfile() {
    export DB_KEYFILE="${DB_PATH}/mongod.key"
    rm -rfv "${DB_KEYFILE}"
    mkdir -pv "${DB_PATH}"
    openssl rand -base64 755 >"${DB_KEYFILE}"
    chmod -c 400 "${DB_KEYFILE}"
}
export -f m310_create_keyfile

function m310_create_encryption_key() {
    export DB_ENCRYPTION_KEY="${DB_PATH}/mongod-data.key"
    rm -rfv "${DB_ENCRYPTION_KEY}"
    mkdir -pv "${DB_PATH}"
    openssl rand -base64 755 >"${DB_ENCRYPTION_KEY}"
    chmod -c 400 "${DB_ENCRYPTION_KEY}"
}
export -f m310_create_encryption_key

function m310_setup_rs_tls() {
    local CA_CERT=$1
    local SERVER_CERT=$2
    local CLIENT_CERT=$3
    MONGOD_ARGS="${MONGOD_ARGS}
        --sslMode=requireSSL
        --sslCAFile=${CA_CERT}
        --sslPEMKeyFile=${SERVER_CERT}
        "
    MONGO_ARGS="${MONGO_ARGS}
        --ssl
        --sslCAFile=${CA_CERT}
        --sslPEMKeyFile=${CLIENT_CERT}
        "
    echo "Current MONGOD_ARGS: ${MONGOD_ARGS}"
    echo "Current MONGO_ARGS: ${MONGO_ARGS}"
}
export -f m310_setup_rs_tls

function m310_enable_auditing() {
    MONGOD_AUDITING_ARGS="
        --auditDestination=file \
        --auditFormat=JSON
        "
    echo "Current MONGOD_AUDITING_ARGS: ${MONGOD_AUDITING_ARGS}"
}
export -f m310_enable_auditing

function m310_add_audit_filter() {
    FILTER=$1
    MONGOD_AUDIT_FILTERS="${MONGOD_AUDIT_FILTERS}
        --auditFilter '${FILTER}'
    "
    echo "Current MONGOD_AUDIT_FILTERS: ${MONGOD_AUDIT_FILTERS}"
}
export -f m310_add_audit_filter
