#!/usr/bin/env bash

# Although this homework assignment comes with a setup script, I believe I can
# get the answer without it.  Looking at the validation script it doesn't look
# like it will need anything specific from the supplied setup script.

HW=2.5
SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

m310_print_header 'Unzipping homework validation script.'
HW_ZIP="../chapter_2_authorization_and_encryption.51f62f67fb7d.zip"
HW_SETUP_SCRIPT="setup-hw-${HW}.sh"
HW_VALIDATION_SCRIPT="validate-hw-${HW}.sh"
rm -rfv "${HW_SETUP_SCRIPT}"
rm -rfv "${HW_VALIDATION_SCRIPT}"
unzip -j "${HW_ZIP}" "m310-hw-${HW}/${HW_SETUP_SCRIPT}"
unzip -j "${HW_ZIP}" "m310-hw-${HW}/${HW_VALIDATION_SCRIPT}"

m310_print_header '"Tweeking" the setup script.'
sed -i -e '/hostname=`hostname -f`/ s/=.*/=localhost/' "${HW_SETUP_SCRIPT}"

m310_print_header 'Setting up the replica-set via the supplied setup script.'
# Change the home directory to the cwd so that DB files are local.
ORIGINAL_HOME=${HOME}
HOME=.
bash -c "./${HW_SETUP_SCRIPT}"
HOME=${ORIGINAL_HOME}

m310_print_header 'Determining primary and secondary nodes.'
declare -a DB_PORTS=( 31250               31251               31252 )
m310_determine_primary_node ${DB_PORTS[@]}
DB_SECONDARY_PORTS=$(m310_print_secondary_nodes ${DB_PORTS[@]})
declare -a DB_SECONDARY_PORTS=( ${DB_SECONDARY_PORTS} )
echo "Secondary nodes are:"
for SECONDARY in ${DB_SECONDARY_PORTS[@]} ; do
    echo "    ${SECONDARY}"
done

m310_print_header 'Setting up replica-set initialization strings... duplicated from setup script.'
replSetName="UNENCRYPTED"
host=localhost
declare -a ports=( ${DB_PORTS[@]} )
initiateStr="rs.initiate({
                 _id: '$replSetName',
                 members: [
                  { _id: 1, host: '$host:${ports[0]}' },
                  { _id: 2, host: '$host:${ports[1]}' },
                  { _id: 3, host: '$host:${ports[2]}' }
                 ]
                })"

m310_print_header 'Setting up replica-set strings custom to this script cause the fucking setup script sucks.'
DB_REPL_SET_NAME=${replSetName}

m310_print_header 'STEP: 1. Create a data encryption key.'
m310_create_encryption_key

m310_print_header 'STEP: 2. Safely shutdown a secondary of the replica set and delete the old database files.'
SECONDARY=${DB_SECONDARY_PORTS[0]}
mongo --port ${SECONDARY} <<EOF
    use admin;
    db.shutdownServer()
EOF
sleep 5s

m310_print_header 'STEP: 3. Restart the server with storage encryption enabled.'
pwd
m310_start_single_mongod ${SECONDARY}
mongo --port ${SECONDARY} --eval "${initiateStr}"
sleep 5s

read



m310_print_header 'STEP: 4. Repeat steps 2 and 3 for the other secondary.'
SECONDARY=${DB_SECONDARY_PORTS[1]}
mongo --port ${SECONDARY} <<EOF
    use admin;
    db.shutdownServer();
EOF
m310_start_single_mongod ${SECONDARY}
mongo --port ${SECONDARY} --eval "${initiateStr}"
sleep 5s

m310_print_header 'STEP: 5. Step down the primary and repeat steps 2 and 3 on the former primary.'
mongo --port ${DB_PRIMARY_PORT} <<EOF
    use admin;
    db.shutdownServer({force: true});
EOF
m310_start_single_mongod ${DB_PRIMARY_PORT}
sleep 5s

m310_print_header 'Validating environment.'
ANSWER=$(bash -c "./${HW_VALIDATION_SCRIPT}")

m310_print_header 'Shutting down replica-set.'
m310_destroy_local_rs ${DB_PORTS[@]}

m310_print_header 'Cleaning up the last remaining homework files.'
rm -rfv "${HW_SETUP_SCRIPT}"
rm -rfv "${HW_VALIDATION_SCRIPT}"
rm -rf "M310-HW-2.5"

m310_print_header 'And the answer is:  (paste this into the homework assignment)'
echo ${ANSWER}
