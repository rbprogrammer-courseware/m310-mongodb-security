#!/usr/bin/env bash

HW=2.4
SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

m310_print_header 'Unzipping homework supplies.'
HW_ZIP="../chapter_2_authorization_and_encryption.51f62f67fb7d.zip"
HW_VALIDATION_SCRIPT="validate-hw-${HW}.sh"
rm -rfv "${HW_VALIDATION_SCRIPT}"
unzip -j "${HW_ZIP}" "m310-hw-${HW}/${HW_VALIDATION_SCRIPT}"
unzip -j -d certs "${HW_ZIP}" "m310-certs/*" <<EOF
A
EOF
find certs/ -type f | xargs chmod -c 777

m310_print_header '"Tweeking" the validation script.'
sed -i -e '/hostname=`hostname -f`/ s/=.*/=localhost/' "${HW_VALIDATION_SCRIPT}"

m310_print_header 'Setting up SSL/TLS certs.'
CA_CERT="${SCRIPT_DIR}/certs/ca.pem"
SERVER_CERT="${SCRIPT_DIR}/certs/server.pem"
CLIENT_CERT="${SCRIPT_DIR}/certs/client.pem"
m310_setup_rs_tls "${CA_CERT}" "${SERVER_CERT}" "${CLIENT_CERT}"

m310_print_header 'Starting replica-set.'
declare -a DB_PORTS=( 31240       31241       31242 )
m310_start_local_rs ${DB_PORTS[@]}

m310_print_header 'Initiating replica-set.'
m310_initiate_rs ${DB_PORTS[@]}

m310_print_header 'Determining which node is the primary.'
m310_determine_primary_node ${DB_PORTS[@]}

m310_print_header 'Validating environment.'
# Need to set up soft-link so the validation script finds the certs.
ln -vs $(pwd) ~/shared
ANSWER=$(bash -c "./${HW_VALIDATION_SCRIPT}")
unlink ~/shared

m310_print_header 'Shutting down replica-set.'
m310_destroy_local_rs ${DB_PORTS[@]}

m310_print_header 'Cleaning up the last remaining homework files.'
rm -rfv "${HW_VALIDATION_SCRIPT}"
rm -rfv certs

m310_print_header 'And the answer is:  (paste this into the homework assignment)'
echo ${ANSWER}
