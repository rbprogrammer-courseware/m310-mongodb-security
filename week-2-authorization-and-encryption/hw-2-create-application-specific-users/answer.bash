#!/usr/bin/env bash

HW=2.2
SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

m310_print_header 'Unzipping homework validation script.'
HW_ZIP="../chapter_2_authorization_and_encryption.51f62f67fb7d.zip"
HW_VALIDATION_SCRIPT="validate-hw-${HW}.sh"
rm -rfv "${HW_VALIDATION_SCRIPT}"
unzip -j "${HW_ZIP}" "m310-hw-${HW}/${HW_VALIDATION_SCRIPT}"

m310_print_header 'Starting replica-set.'
declare -a DB_PORTS=( 31220       31221       31222 )
m310_start_local_rs ${DB_PORTS[@]}

m310_print_header 'Initiating replica-set.'
m310_initiate_rs ${DB_PORTS[@]}

m310_print_header 'Determining which node is the primary.'
m310_determine_primary_node ${DB_PORTS[@]}

m310_print_header 'Creating users: docs.mongodb.com/manual/reference/built-in-roles'
mongo --port="${DB_PRIMARY_PORT}" <<EOF
    use admin;
    db.createUser({ user: 'admin',
                    pwd: 'webscale',
                    roles: [{role: 'root', db: 'admin'}]
    })
    db.auth('admin', 'webscale')
    db.createUser({ user: 'reader',
                    pwd: 'books',
                    roles: [{role: 'read', db: 'acme'}]
    })
    db.createUser({ user: 'writer',
                    pwd: 'typewriter',
                    roles: [{role: 'readWrite', db: 'acme'}]
    })
EOF

m310_print_header 'Validating environment.'
ANSWER=$(bash -c "./${HW_VALIDATION_SCRIPT}")

m310_print_header 'Shutting down replica-set.'
m310_destroy_local_rs ${DB_PORTS[@]}

m310_print_header 'Cleaning up the last remaining homework files.'
rm -rfv "${HW_VALIDATION_SCRIPT}"

m310_print_header 'And the answer is:  (paste this into the homework assignment)'
echo ${ANSWER}
