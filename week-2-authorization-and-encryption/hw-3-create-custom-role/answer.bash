#!/usr/bin/env bash

HW=2.3
SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

# This homework doesn't need to really set up a replica-set.  So the timeout can
# be really short.
HW_SCRIPT_TIMEOUT=5s

m310_print_header 'Unzipping homework validation script.'
HW_ZIP="../chapter_2_authorization_and_encryption.51f62f67fb7d.zip"
HW_VALIDATION_SCRIPT="validate-hw-${HW}.sh"
rm -rfv "${HW_VALIDATION_SCRIPT}"
unzip -j "${HW_ZIP}" "m310-hw-${HW}/${HW_VALIDATION_SCRIPT}"

m310_print_header 'Starting replica-set.'
declare -a DB_PORTS=( 31230 )
m310_start_local_rs ${DB_PORTS[@]}

m310_print_header 'Initiating replica-set.'
m310_initiate_rs ${DB_PORTS[@]}

m310_print_header 'Determining which node is the primary.'
m310_determine_primary_node ${DB_PORTS[@]}

m310_print_header 'Creating privileges: docs.mongodb.com/manual/reference/privilege-actions'
mongo --port="${DB_PRIMARY_PORT}" <<EOF
    use admin;

    db.createRole({
        role: 'HRDEPARTMENT',
        privileges: [{
            resource: { db: 'HR', collection: '' },
            actions: [ 'find', 'remove' ]
        }, {
            resource: { db: 'HR',  collection: 'employees' },
            actions: [ 'insert' ]
        }],
        roles: [],
    })

    db.createRole({
        role: 'MANAGEMENT',
        privileges: [{
            resource: { db: 'HR', collection: '' },
            actions: [ 'insert' ]
        }],
        roles: [{ role: 'dbOwner', db: 'HR' }],
    })

    db.createRole({
        role: 'EMPLOYEEPORTAL',
        privileges: [{
            resource: { db: 'HR',  collection: 'employees' },
            actions: [ 'find', 'update' ]
        }],
        roles: [],
    })
EOF

m310_print_header 'Validating environment.'
ANSWER=$(bash -c "./${HW_VALIDATION_SCRIPT}")

m310_print_header 'Shutting down replica-set.'
m310_destroy_local_rs ${DB_PORTS[@]}

m310_print_header 'Cleaning up the last remaining homework files.'
rm -rfv "${HW_VALIDATION_SCRIPT}"

m310_print_header 'And the answer is:  (paste this into the homework assignment)'
echo ${ANSWER}







exit

All of these options are fucking incorrect.

[{"role":"EMPLOYEEPORTAL","inheritedRoles":[],"privileges":[{"resource":{"db":"HR","collection":"employees"},"actions":["find","update"]}]},{"role":"HRDEPARTMENT","inheritedRoles":[],"privileges":[{"resource":{"db":"HR","collection":""},"actions":["find","remove"]},{"resource":{"db":"HR","collection":"employees"},"actions":["insert"]}]},{"role":"MANAGEMENT","inheritedRoles":[{"role":"dbOwner","db":"HR"}],"privileges":[{"resource":{"db":"HR","collection":""},"actions":["insert"]}]}]
[{"role":"EMPLOYEEPORTAL","inheritedRoles":[],"privileges":[{"resource":{"db":"HR","collection":"employees"},"actions":["find","update"]}]},{"role":"HRDEPARTMENT","inheritedRoles":[],"privileges":[{"resource":{"db":"HR","collection":""},"actions":["find","remove"]},{"resource":{"db":"HR","collection":"employees"},"actions":["insert","update"]}]},{"role":"MANAGEMENT","inheritedRoles":[{"role":"dbOwner","db":"HR"}],"privileges":[{"resource":{"db":"HR","collection":""},"actions":["insert"]}]}]
[{"role":"EMPLOYEEPORTAL","inheritedRoles":[],"privileges":[{"resource":{"db":"HR","collection":"employees"},"actions":["find","update"]}]},{"role":"HRDEPARTMENT","inheritedRoles":[],"privileges":[{"resource":{"db":"HR","collection":""},"actions":["find","remove"]},{"resource":{"db":"HR","collection":"employees"},"actions":["insert"]}]},{"role":"MANAGEMENT","inheritedRoles":[{"role":"dbOwner","db":"HR"}],"privileges":[{"resource":{"db":"HR","collection":""},"actions":["insert"]}]}]
