#!/usr/bin/env bash

HW=3.2
SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

m310_print_header 'Unzipping homework validation script.'
HW_ZIP="../chapter_3_auditing_and_best_practices.440b1fb4c925.zip"
HW_VALIDATION_SCRIPT="validate-hw-${HW}.sh"
rm -rfv "${HW_VALIDATION_SCRIPT}"
unzip -j "${HW_ZIP}" "m310-hw-${HW}/${HW_VALIDATION_SCRIPT}"

m310_print_header 'Enabling auditing.'
m310_enable_auditing

m310_print_header 'Adding audit filters: docs.mongodb.com/manual/tutorial/configure-audit-filters'
m310_add_audit_filter '{ "users.user": "steve" }'

m310_print_header 'Starting replica-set'
declare -a DB_PORTS=( 31320       31321       31322 )
m310_start_local_rs ${DB_PORTS[@]}

m310_print_header 'Initiating replica-set.'
m310_initiate_rs ${DB_PORTS[@]}

m310_print_header 'Determining which node is the primary.'
m310_determine_primary_node ${DB_PORTS[@]}

m310_print_header 'Creating user: docs.mongodb.com/manual/reference/method/db.createUser'
mongo --port="${DB_PRIMARY_PORT}" <<EOF
    use admin;
    db.createUser({ user: 'steve',
                    pwd: 'secret',
                    roles: ['root']
    })
    db.auth('steve', 'secret')
EOF

m310_print_header 'Validating environment.'
# Need to set up soft-link so the validation script finds the audit logs.
ln -sv "$(pwd)/${DB_PATH}" ~/M310-HW-${HW}
ANSWER=$(bash -c "./${HW_VALIDATION_SCRIPT}")
unlink ~/M310-HW-${HW}

m310_print_header 'Shutting down replica-set.'
m310_destroy_local_rs ${DB_PORTS[@]}

m310_print_header 'Cleaning up the last remaining homework files.'
rm -rfv "${HW_VALIDATION_SCRIPT}"

m310_print_header 'And the answer is:  (paste this into the homework assignment)'
echo ${ANSWER}
