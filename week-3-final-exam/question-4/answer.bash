#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

m310_print_header 'Setting up environment.'
DB_NAME=production
function m310_final4_create_role() {
    ROLE_DOC="$@"
    mongo --quiet ${DB_NAME} <<EOF
        use ${DB_NAME};
        db.createRole(${ROLE_DOC});
EOF
}

function m310_final4_check_success() {
    SUCCESS=$1
    echo
    echo -n 'Test is a: '
    if [[ "x${SUCCESS}" == "x0" ]] ; then
        echo 'SUCCESS.'
    else
        echo 'FAILURE!'
    fi
}

m310_print_header 'Setting up single server instance.'
mongod --fork --dbpath=${DB_PATH} --syslog

m310_print_header 'Checking document #1'
DOC='
{
  role: "team-lead",
  privileges: [{
    resource: { replicaSet: true },
    actions: [ "createUser" ]
  }],
  roles:[{
    role: "root", db: "production"
  }]
}
'
m310_final4_create_role ${DOC}
m310_final4_check_success $?

m310_print_header 'Checking document #2'
DOC='
{
  role: "senior-engineer",
  privileges: [],
  roles: [ "dbAdmin" ]
}
'
m310_final4_create_role ${DOC}
m310_final4_check_success $?

m310_print_header 'Checking document #3'
DOC='
{
  role: "junior-engineer",
  privileges: [{
    resource: { db: "production" },
    actions: [ "insert" ]
  }],
  roles:[]
}
'
m310_final4_create_role ${DOC}
m310_final4_check_success $?

m310_print_header 'Checking document #4'
DOC='
{
  role: "intern",
  privileges: [{
    resource: { db: "staging", collection: "products" },
    actions: [ "insert" ]
  }],
  roles:[]
}
'
m310_final4_create_role ${DOC}
m310_final4_check_success $?

m310_print_header 'Cleaning up the mess.'
mongod --shutdown --port=27017
killall -v mongod
rm -rf "${DB_PATH}"
