#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash
rm -rfv "${DB_PATH}"

m310_print_header 'Unzipping homework validation script.'
ZIP="m310-certs.6401b189f25c.zip"
CLIENT_PEM='client.pem'
unzip "${ZIP}" "${CLIENT_PEM}"

m310_print_header 'Determining the client certificate information.'
ANSWER="$(openssl x509 -inform PEM -in "${CLIENT_PEM}" -text -nameopt RFC2253)"

m310_print_header 'Cleaning up the exam question files.'
rm -rfv "${CLIENT_PEM}"

m310_print_header 'And the answer is:  (paste this into the homework assignment)'
echo "${ANSWER}"
