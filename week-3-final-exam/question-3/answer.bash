#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

m310_print_header 'Setting up environment'
DB_PORT=27017
DB_AUDIT_FILTER='
    {
      "$or": [
        {
          "atype": "authCheck",
          "param.command": {
            "$in": [
              "find",
              "insert",
              "delete",
              "update",
              "findandmodify"
            ]
          }
        },
        {
          "atype": {
            "$in": [
              "createCollection",
              "dropCollection"
            ]
          }
        }
      ]
    }
'
DB_AUDIT_FILE=${DB_PATH}/audit.json

function m310_final3_start_server() {
    mkdir -pv ${DB_PATH}
    mongod \
        --fork \
        --port=${DB_PORT} \
        --dbpath=${DB_PATH} \
        --syslog \
        --auditDestination=file \
        --auditFormat=JSON \
        --auditPath=${DB_AUDIT_FILE} \
        --auditFilter "${DB_AUDIT_FILTER}"
}

function m310_final3_stop_server() {
    mongod --shutdown --dbpath=${DB_PATH}
    killall -v mongod
    rm -rf "${DB_PATH}"
}

function m310_final3_check_operation() {
    OPERATION="$@"

    echo 'Starting server.'
    m310_final3_start_server

    PRE_CHECK_COUNT=$(wc -l ${DB_AUDIT_FILE} | cut -d' ' -f 1)
    mongo --quiet --port=${DB_PORT} <<EOF
        ${OPERATION}
EOF
    sleep 4s
    POST_CHECK_COUNT=$(wc -l ${DB_AUDIT_FILE} | cut -d' ' -f 1)

    echo 'Stopping server.'
    m310_final3_stop_server

    echo
    echo -n '===> ANSWER: Operation did '
    if [[ "${POST_CHECK_COUNT}" == "${PRE_CHECK_COUNT}" ]] ; then
        echo -n 'NOT '
    fi
    echo 'log an audit event.'

    sleep 2s
}

m310_print_header 'Checking operation #1.'
m310_final3_check_operation "show dbs"

m310_print_header 'Checking operation #2.'
m310_final3_check_operation "db.products.insert({product: 'Amplifier'})"

m310_print_header 'Checking operation #3.'
m310_final3_check_operation "db.products.insertOne({product: 'Basket'})"

m310_print_header 'Checking operation #4.'
m310_final3_check_operation "db.products.find({product: 'Candle'})"

m310_print_header 'Checking operation #5.'
m310_final3_check_operation "db.products.findOne({product: 'Door Hinge'})"
