Which of the following statements is/are true?

Check all that apply:
[x] The subject of a client certificate acts as the "user" when authenticating
    with X.509 certificates.
[ ] The preferSSL SSL mode allows the server to accept both TLS and non-TLS
    connections between clients and other members.
[x] When enabling internal authentication between the members of a replica set
    both certificate and key must be present in the CA, client, and server PEM
    files.
[x] MongoDB stores user-defined role information in the system.roles collection
    in the admin database.
[x] When auditing is enabled on MongoDB Enterprise, the --auditFormat BSON
    option has much better performance than the --auditFormat JSON option.
