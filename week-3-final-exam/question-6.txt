Which of the following statements is/are true?

Check all that apply:
[ ] Internal authentication via X.509 certificates will enable MongoDB's
    role-based access control authorization system.
[x] The localhost exception still applies to members of a replica set or sharded
    cluster.
[ ] Audit logs can go to one of four locations: the system log, the console, to
    another MongoDB member, or to a file.
[x] Encryption at rest is a four step process: generate a master key, generate
    keys for each database, encrypt each database with the database keys, and
    encrypt the database keys with the master key.
[ ] When you enable encryption at rest, transport encryption between replicating
    members is automatically enabled.
