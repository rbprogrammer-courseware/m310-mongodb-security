#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

m310_print_header 'Unzipping homework data.'
HW_SCRIPTS_DIR='m310-hw-1.4'
HW_VAGRANT_DIR='m310-vagrant-env'
rm -rf "${HW_SCRIPTS_DIR}"
rm -rf "${HW_VAGRANT_DIR}"
unzip ../chapter_1_authentication.fd60fc419fad.zip "${HW_SCRIPTS_DIR}/*"
unzip ../chapter_1_authentication.fd60fc419fad.zip "${HW_VAGRANT_DIR}/*"

m310_print_header 'Setting up Vagrant environment.'
HW_SETUP_SCRIPT='setup-hw-1.4.sh'
HW_VALIDATION_SCRIPT='validate-hw-1.4.sh'
mkdir -p "${HW_VAGRANT_DIR}/shared"
mv -v "${HW_SCRIPTS_DIR}/${HW_SETUP_SCRIPT}" "${HW_VAGRANT_DIR}/shared/"
mv -v "${HW_SCRIPTS_DIR}/${HW_VALIDATION_SCRIPT}" "${HW_VAGRANT_DIR}/shared/"
rm -rfv "${HW_SCRIPTS_DIR}"
chmod 755 -c "${HW_VAGRANT_DIR}/shared/*.sh"

m310_print_header 'Setting up VMs'
pushd "${HW_VAGRANT_DIR}"
vagrant plugin install vagrant-vbguest
vagrant up

m310_print_header 'Setting up the MongoDB environment.'
vagrant ssh database <<EOF
    bash -c "shared/${HW_SETUP_SCRIPT}"
EOF

m310_print_header 'Converting the database from MONGODB-CR to SCRAM-SHA-1.'
vagrant ssh database <<EOF_SSH
    mongo <<EOF_MONGO
        use admin;
        db.auth('alice', 'secret');
        db.system.users.find().pretty();
        db.adminCommand({authSchemaUpgrade: 1});
        db.system.users.find().pretty();
EOF_MONGO
EOF_SSH

m310_print_header 'Validating environment => THE ANSWER WILL BE THE LAST LINE IN THIS SECTION!!!!!!'
vagrant ssh database <<EOF
    bash -c "shared/${HW_VALIDATION_SCRIPT}"
EOF

m310_print_header 'Cleaning up VMs'
vagrant -f destroy
popd

m310_print_header 'Cleaning up Vagrant environment.'
rm -rfv "${HW_VAGRANT_DIR}"
