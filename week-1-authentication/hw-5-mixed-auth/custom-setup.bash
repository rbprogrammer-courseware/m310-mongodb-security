#!/usr/bin/env bash

DB_HOST=$(hostname -f)
DB_LOG_NAME='mongod.log'
DB_PORTS=(31150 31151 31152)
DB_REPL_SET_NAME='MY_REPL_SET'
DB_REPL_SET_INITIALIZER="rs.initiate({
                        _id: '${DB_REPL_SET_NAME}',
                        members: [
                          { _id: 1, host: '${DB_HOST}:${DB_PORTS[0]}' },
                          { _id: 2, host: '${DB_HOST}:${DB_PORTS[1]}' },
                          { _id: 3, host: '${DB_HOST}:${DB_PORTS[2]}' }
                        ]
                        })"
DB_WORKING_DIR='/home/vagrant/M310-HW-1.5'
HW_SHARED_DIR='/home/vagrant/shared'
KEYFILE="${HW_SHARED_DIR}/mongod.key"
CERT_DIR="${HW_SHARED_DIR}/certs"
CERT_CA="${CERT_DIR}/ca.pem"
CERT_SERVER="${CERT_DIR}/server.pem"
CERT_CLIENT="${CERT_DIR}/client.pem"
CERT_CLIENT_CN="$(openssl x509 -in "${CERT_CLIENT}" -inform PEM -subject -nameopt RFC2253 -noout | sed 's/subject= //')"
sleep 4s

cd

echo
echo 'Initialize each database in the replica set.'
for (( i=0; i < ${#DB_PORTS[@]}; i++ )) ; do
    mkdir -pv "${DB_WORKING_DIR}/rs${i}/db"
    mongod --fork \
        --dbpath="${DB_WORKING_DIR}/rs${i}/db" \
        --logpath="${DB_WORKING_DIR}/rs${i}/${DB_LOG_NAME}" \
        --bind_ip="${DB_HOST}" \
        --port=${DB_PORTS[${i}]} \
        --replSet="${DB_REPL_SET_NAME}" \
        --keyFile="${KEYFILE}"
    sleep 5s
done
sleep 20s

echo
echo 'Initialize the replica set.'
mongo --host="${DB_HOST}" --port="${DB_PORTS[0]}" --eval "${DB_REPL_SET_INITIALIZER}"
sleep 20s

echo
echo 'Create the user on all databases since we dont know which is the PRIMARY.'
for ((i=0; i < ${#DB_PORTS[@]}; i++)) ; do
    mongo --host="${DB_HOST}" --port="${DB_PORTS[${i}]}" <<EOF
        use admin;
        db.createUser({user:'will', pwd: '\$uperAdmin', roles: ['root']});
        db.auth('will', '\$uperAdmin');
        db.getSisterDB('\$external').runCommand({
            createUser: "${CERT_CLIENT_CN}",
            roles: [{role: 'userAdminAnyDatabase', db: 'admin'}]
        });
EOF
    sleep 5s
done

echo
echo 'Shutdown the replica set.'
for ((i=0; i < ${#DB_PORTS[@]}; i++)) ; do
    mongod --shutdown --dbpath="${DB_WORKING_DIR}/rs${i}/db"
done
killall mongod

echo
echo 'Restart with authentication enabled.'
for ((i=0; i < ${#DB_PORTS[@]}; i++)) ; do
    mongod --fork \
        --dbpath="${DB_WORKING_DIR}/rs${i}/db" \
        --logpath="${DB_WORKING_DIR}/rs${i}/${DB_LOG_NAME}" \
        --bind_ip="${DB_HOST}" \
        --port=${DB_PORTS[${i}]} \
        --replSet="${DB_REPL_SET_NAME}" \
        --sslMode="requireSSL" \
        --sslCAFile="${CERT_CA}" \
        --sslPEMKeyFile="${CERT_SERVER}" \
        --keyFile="${KEYFILE}"
    sleep 10s
done





exit

#HW_DIR=/home/vagrant/M310-HW-1.5
#mongod --shutdown --dbpath=${HW_DIR}/rs0/db
#mongod --shutdown --dbpath=${HW_DIR}/rs1/db
#mongod --shutdown --dbpath=${HW_DIR}/rs2/db
#killall mongod
#rm -rf ${HW_DIR}

