#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

m310_print_header 'Unzipping homework data.'
HW_CERTS_DIR='m310-certs'
HW_SCRIPTS_DIR='m310-hw-1.5'
HW_VAGRANT_DIR='m310-vagrant-env'
HW_SHARED_DIR="${HW_VAGRANT_DIR}/shared"
rm -rfv "${HW_CERTS_DIR}"
rm -rfv "${HW_SCRIPTS_DIR}"
rm -rfv "${HW_VAGRANT_DIR}"
rm -rfv "${HW_SHARED_DIR}"
unzip ../chapter_1_authentication.fd60fc419fad.zip "${HW_CERTS_DIR}/*" <<EOF
A
EOF
unzip ../chapter_1_authentication.fd60fc419fad.zip "${HW_SCRIPTS_DIR}/*"
unzip ../chapter_1_authentication.fd60fc419fad.zip "${HW_VAGRANT_DIR}/*"

m310_print_header 'Setting up Vagrant environment.'
HW_SETUP_SCRIPT='custom-setup.bash'
HW_VALIDATION_SCRIPT="validate-hw-1.5.sh"
mkdir -pv "${HW_SHARED_DIR}"
mv -v "${HW_CERTS_DIR}" "${HW_SHARED_DIR}/certs"
mv -v "${HW_SCRIPTS_DIR}/${HW_VALIDATION_SCRIPT}" "${HW_SHARED_DIR}/"
rm -rfv "${HW_SCRIPTS_DIR}"
cp -v "${HW_SETUP_SCRIPT}" "${HW_SHARED_DIR}/"
chmod 755 "${HW_SHARED_DIR}/${HW_SETUP_SCRIPT}"

m310_print_header 'Setting up VMs'
pushd "${HW_VAGRANT_DIR}"
vagrant plugin install vagrant-vbguest
vagrant up

m310_print_header 'Creating a common key-file for a new Mongo replica-set.'
KEYFILE='shared/mongod.key'
openssl rand -base64 755 >"${KEYFILE}"
chmod 600 "${KEYFILE}"

m310_print_header 'Setting up Mongo replica-set.'
vagrant ssh database <<EOF
    bash -c "shared/${HW_SETUP_SCRIPT}"
EOF

exit

m310_print_header 'Validating environment.'
echo 'NOTE:   IF THIS DOES NOT WORK IT IS BECAUSE THE VALIDATION SCRIPT'
echo 'NOTE:   ATTEMPTS TO CONNECT TO PORT 31150, WHICH MIGHT NOT BE THE'
echo 'NOTE:   PRIMARY.  IF YOU DO NOT SUSPECT IT WORKING THEN YOULL NEED TO'
echo 'NOTE:   CHANGE THE PORT IN THE VALIDATION SCRIPT ON THE DATABASE VM'
echo 'NOTE:   BEFORE IT GETS RAN.'
echo 'NOTE:'
echo 'NOTE:   I DONT KNOW HOW TO MAKE THIS ANY BETTER.'
vagrant ssh database <<EOF
    bash -c "shared/${HW_VALIDATION_SCRIPT}"
EOF

m310_print_header 'Cleaning up VMs'
vagrant -f destroy
popd

m310_print_header 'Cleaning up Vagrant environment.'
rm -rfv "${HW_VAGRANT_DIR}"
