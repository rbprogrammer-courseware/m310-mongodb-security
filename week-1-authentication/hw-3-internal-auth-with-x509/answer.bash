#!/usr/bin/env bash

# FIXME this script isn't 100% complete, but it's mostly there to get the answer.

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

m310_print_header 'Unzipping homework data.'
HW_CERTS_DIR='m310-certs'
HW_SCRIPTS_DIR='m310-hw-1.3'
HW_VAGRANT_DIR='m310-vagrant-env'
rm -rf "${HW_CERTS_DIR}"
rm -rf "${HW_SCRIPTS_DIR}"
rm -rf "${HW_VAGRANT_DIR}"
unzip ../chapter_1_authentication.fd60fc419fad.zip "${HW_CERTS_DIR}/*" <<EOF
A
EOF
unzip ../chapter_1_authentication.fd60fc419fad.zip "${HW_SCRIPTS_DIR}/*"
unzip ../chapter_1_authentication.fd60fc419fad.zip "${HW_VAGRANT_DIR}/*"

m310_print_header 'Setting up Vagrant environment.'
HW_SETUP_SCRIPT='setup.bash'
HW_VALIDATION_SCRIPT="validate-hw-1.3.sh"
mkdir -p "${HW_VAGRANT_DIR}/shared"
mv -v "${HW_CERTS_DIR}/" "${HW_VAGRANT_DIR}/shared/certs"
mv -v "${HW_SCRIPTS_DIR}/${HW_VALIDATION_SCRIPT}" "${HW_VAGRANT_DIR}/shared/"
cp -v "${HW_SETUP_SCRIPT}" "${HW_VAGRANT_DIR}/shared/"
chmod 755 "${HW_VAGRANT_DIR}/shared/${HW_SETUP_SCRIPT}"
rm -rfv "${HW_SCRIPTS_DIR}"

m310_print_header 'Setting up VMs'
pushd "${HW_VAGRANT_DIR}"
vagrant plugin install vagrant-vbguest
vagrant up

m310_print_header 'Setting up Mongo replica-set.'
vagrant ssh database <<EOF
    bash -c "shared/${HW_SETUP_SCRIPT}"
EOF

m310_print_header 'Validating environment.'
vagrant ssh database <<EOF
    bash -c "shared/${HW_VALIDATION_SCRIPT}"
EOF

m310_print_header 'Cleaning up VMs'
vagrant -f destroy
popd

m310_print_header 'Cleaning up Vagrant environment.'
rm -rfv "${HW_VAGRANT_DIR}"
