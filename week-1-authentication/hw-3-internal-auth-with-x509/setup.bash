#!/usr/bin/env bash

DB_HOST=$(hostname -f)
DB_LOG_NAME='mongod.log'
DB_PORTS=(31130 31131 31132)
DB_REPL_SET_NAME='MY_REPL_SET'
DB_REPL_SET_INITIALIZER="rs.initiate({
                        _id: '${DB_REPL_SET_NAME}',
                        members: [
                          { _id: 1, host: '${DB_HOST}:${DB_PORTS[0]}' },
                          { _id: 2, host: '${DB_HOST}:${DB_PORTS[1]}' },
                          { _id: 3, host: '${DB_HOST}:${DB_PORTS[2]}' }
                        ]
                        })"
DB_WORKING_DIR=/home/vagrant/m310
CERT_DIR="${DB_WORKING_DIR}/../shared/certs"
CERT_CA="${CERT_DIR}/ca.pem"
CERT_SERVER="${CERT_DIR}/server.pem"
CERT_CLIENT="${CERT_DIR}/client.pem"
CERT_CLIENT_CN="$(openssl x509 -in ${CERT_CLIENT} -inform PEM -subject -nameopt RFC2253 -noout | sed 's/subject= //')"

cd

echo
echo 'Initialize each database in the replica set.'
for (( i=0; i < ${#DB_PORTS[@]}; i++ )) ; do
    mkdir -pv "${DB_WORKING_DIR}/rs${i}/db"
    mongod --fork \
        --dbpath="${DB_WORKING_DIR}/rs${i}/db" \
        --logpath="${DB_WORKING_DIR}/rs${i}/${DB_LOG_NAME}" \
        --port=${DB_PORTS[${i}]} \
        --replSet="${DB_REPL_SET_NAME}"
    sleep 5
done
sleep 20s

echo
echo 'Initialize the replica set.'
mongo --port ${DB_PORTS[0]} --eval "${DB_REPL_SET_INITIALIZER}"
sleep 10s

echo
echo 'Create the user.'
mongo --port="${DB_PORTS[0]}" <<EOF
    db.getSisterDB('\$external').runCommand({
        createUser: "${CERT_CLIENT_CN}",
        roles: [{role: 'root', db: 'admin'}]
    })
EOF

echo
echo 'Shutdown the replica set.'
for ((i=0; i < ${#DB_PORTS[@]}; i++)) ; do
    mongod --shutdown --dbpath="${DB_WORKING_DIR}/rs${i}/db"
done
killall mongod

echo
echo 'Restart with authentication enabled.'
for ((i=0; i < ${#DB_PORTS[@]}; i++)) ; do
    mongod --fork \
        --dbpath="${DB_WORKING_DIR}/rs${i}/db" \
        --logpath="${DB_WORKING_DIR}/rs${i}/${DB_LOG_NAME}" \
        --port=${DB_PORTS[${i}]} \
        --replSet="${DB_REPL_SET_NAME}" \
        --sslMode="requireSSL" \
        --clusterAuthMode="x509" \
        --sslCAFile="${CERT_CA}" \
        --sslPEMKeyFile="${CERT_SERVER}"
    sleep 10s
done





#exit
#
#HW_DIR=/home/vagrant/m310
#mongod --shutdown --dbpath=${HW_DIR}/rs0/db
#mongod --shutdown --dbpath=${HW_DIR}/rs1/db
#mongod --shutdown --dbpath=${HW_DIR}/rs2/db
#killall mongod
#rm -rf ${HW_DIR}

