#!/usr/bin/env bash

# FIXME this script isn't 100% complete, but it's mostly there to get the answer.

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

m310_print_header 'Unzipping homework data.'
VAGRANT_DIR='m310-vagrant-env'
rm -rf "${VAGRANT_DIR}"
unzip ../chapter_1_authentication.fd60fc419fad.zip "${VAGRANT_DIR}/*"

m310_print_header 'Setting up VMs'
pushd "${VAGRANT_DIR}"
vagrant plugin install vagrant-vbguest
vagrant up
popd

m310_print_header 'Setting up mongo database.'
pushd "${VAGRANT_DIR}"
vagrant ssh database <<EOF_SSH
    mkdir db
    mongod --fork --dbpath=db --logpath=db.log
    mongo <<EOF_MONGO
        use admin
        db.createUser({user:'alice', pwd: 'secret', roles: ['root']})
EOF_MONGO
    mongod --dbpath=db --shutdown
    mongod --fork --dbpath=db --logpath=db.log --auth
EOF_SSH
popd

m310_print_header 'Determining if answer #1 is correct.'
pushd "${VAGRANT_DIR}"
vagrant ssh database <<EOF_SSH
    mongo admin \
        -u alice -p secret \
        --eval "db.runCommand({getParameter: 1, authenticationMechanisms: 1})"
EOF_SSH
popd

m310_print_header 'Determining if answer #2 is correct.'
pushd "${VAGRANT_DIR}"
vagrant ssh database <<EOF_SSH
    mongo --eval "db.runCommand({getParameter: 1, authenticationMechanisms: 1})"
EOF_SSH
popd

m310_print_header 'Determining if answer #3 is correct.'
pushd "${VAGRANT_DIR}"
vagrant ssh database <<EOF_SSH
    mongo -u alice -p secret \
        --eval "db.runCommand({getParameter: 1, authenticationMechanisms: 1})"
EOF_SSH
popd

m310_print_header 'Determining if answer #4 is correct.'
pushd "${VAGRANT_DIR}"
vagrant ssh database <<EOF_SSH
    mongo -u alice -p secret \
        --eval "db.runCommand({getParameter: 1, authenticationMechanisms: 1})" \
        --authenticationDatabase admin
EOF_SSH
popd

m310_print_header 'Determining if answer #5 is correct.'
pushd "${VAGRANT_DIR}"
vagrant ssh database <<EOF_SSH
    mongo admin \
        --eval "db.auth('alice', 'secret');db.runCommand({getParameter: 1, authenticationMechanisms: 1})"
EOF_SSH
popd

m310_print_header 'Determining if answer #6 is correct.'
pushd "${VAGRANT_DIR}"
vagrant ssh database <<EOF_SSH
    mongo -u alice -p secret \
        --eval "db=db.getSisterDB('admin');db.runCommand({getParameter: 1, authenticationMechanisms: 1})"
        --authenticationDatabase admin
EOF_SSH
popd

m310_print_header 'Cleaning up VMs'
pushd "${VAGRANT_DIR}"
vagrant --force destroy <<EOF
y
y
EOF
popd

m310_print_header 'Removing vagrant environment.'
rm -rf "${VAGRANT_DIR}"
