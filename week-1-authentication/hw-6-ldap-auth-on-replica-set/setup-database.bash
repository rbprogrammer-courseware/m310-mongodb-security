#!/usr/bin/env bash

# FIXME this script doesn't work!  See fixmes below.

DB_HOST=$(hostname -f)
DB_LOG_NAME='mongod.log'
DB_PORTS=(31160 31161 31162)
DB_REPL_SET_NAME='MY_REPL_SET'
DB_WORKING_DIR='/home/vagrant/M310-HW-1.6'
HW_SHARED_DIR='/home/vagrant/shared'

cd

echo
echo 'Initialize each database in the replica set.'
for (( i=0; i < ${#DB_PORTS[@]}; i++ )) ; do
    mkdir -pv "${DB_WORKING_DIR}/rs${i}/db"
    mongod --fork \
        --dbpath="${DB_WORKING_DIR}/rs${i}/db" \
        --logpath="${DB_WORKING_DIR}/rs${i}/${DB_LOG_NAME}" \
        --bind_ip="${DB_HOST}" \
        --port=${DB_PORTS[${i}]} \
        --replSet="${DB_REPL_SET_NAME}"
    sleep 5s
done
sleep 20s

echo
echo 'Initialize the replica set.'
#mongo --host="${DB_HOST}" --port="${DB_PORTS[0]}" --eval "${DB_REPL_SET_INITIALIZER}"
mongo --host="${DB_HOST}" --port="${DB_PORTS[0]}" <<EOF
    rs.initiate({
        _id: '${DB_REPL_SET_NAME}',
        members: [
            { _id: 1, host: '${DB_HOST}:${DB_PORTS[0]}' },
            { _id: 2, host: '${DB_HOST}:${DB_PORTS[1]}' },
            { _id: 3, host: '${DB_HOST}:${DB_PORTS[2]}' }
        ]
    });
EOF
sleep 30s

echo
echo 'Create the user.'
mongo --host="${DB_HOST}" --port="${DB_PORTS[0]}" <<EOF
    use admin;
    db.getSiblingDB('\$external').createUser({
        user:'adam',
        roles: [{role: 'root', db: 'admin'}]
    });
EOF
sleep 120s

echo
echo 'Shutdown the replica set.'
for ((i=0; i < ${#DB_PORTS[@]}; i++)) ; do
    mongod --shutdown --dbpath="${DB_WORKING_DIR}/rs${i}/db"
done
killall mongod

# FIXME so here is where the replica-set goes from OK to fucked... or a status of "OTHER"
echo
echo 'Restart with authentication enabled.'
for ((i=0; i < ${#DB_PORTS[@]}; i++)) ; do
    mongod --fork \
        --dbpath="${DB_WORKING_DIR}/rs${i}/db" \
        --logpath="${DB_WORKING_DIR}/rs${i}/${DB_LOG_NAME}" \
        --bind_ip="${DB_HOST}" \
        --port=${DB_PORTS[${i}]} \
        --replSet="${DB_REPL_SET_NAME}" \
        --setParameter authenticationMechanisms=PLAIN \
        --setParameter saslauthdPath='/var/run/saslauthd/mux'
    sleep 10s
done

echo
echo 'Test LDAP authentication twice (1 bad pwd, 1 good pwd), should see 0 then 1.'
for ((i=0; i < ${#DB_PORTS[@]}; i++)) ; do
    mongo --host="${DB_HOST}" --port="${DB_PORTS[${i}]}" <<EOF
        use admin;
        db.getSiblingDB('\$external').auth({
            mechanism: 'PLAIN',
            user: 'adam',
            pwd: 'WRONG PASSWORD THAT SHOULD FAIL',
            digestPassword: false
        });
        db.getSiblingDB('\$external').auth({
            mechanism: 'PLAIN',
            user: 'adam',
            pwd: 'password',
            digestPassword: false
        });
EOF
    sleep 5s
done






exit

HW_DIR=/home/vagrant/M310-HW-1.6
mongod --shutdown --dbpath=${HW_DIR}/rs0/db
mongod --shutdown --dbpath=${HW_DIR}/rs1/db
mongod --shutdown --dbpath=${HW_DIR}/rs2/db
killall mongod
rm -rf ${HW_DIR}

