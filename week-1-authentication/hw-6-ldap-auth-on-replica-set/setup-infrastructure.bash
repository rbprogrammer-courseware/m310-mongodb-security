#!/usr/bin/env bash

echo
echo 'Update the saslauthd daemon config.'
sudo sed -i -e '/START=no/ s/=.*/=yes/' /etc/default/saslauthd
sudo sed -i -e '/MECHANISMS="pam"/ s/=.*/="ldap"/' /etc/default/saslauthd

echo
echo 'Point saslauthd to our infrastructure VM.'
SASLAUTHD_CONFIG='/etc/saslauthd.conf'
sudo touch "${SASLAUTHD_CONFIG}"
sudo bash -c "echo 'ldap_servers: ldap://infrastructure.m310.mongodb.university:389' >>'${SASLAUTHD_CONFIG}'"
sudo bash -c "echo 'ldap_search_base: ou=Users,dc=mongodb,dc=com' >>'${SASLAUTHD_CONFIG}'"
sudo bash -c "echo 'ldap_filter: (cn=%u)' >>'${SASLAUTHD_CONFIG}'"
sudo bash -c "echo '' >>'${SASLAUTHD_CONFIG}'"

echo
echo 'Start saslauthd.'
sudo service saslauthd start

echo
echo 'Open the permission on a saslauthd socket file so Mongo can communicate to it.'
SASLAUTHD_SOCK_DIR=/var/run/saslauthd
sudo ls -al "${SASLAUTHD_SOCK_DIR}"
sudo chmod 755 "${SASLAUTHD_SOCK_DIR}"
sudo ls -al "${SASLAUTHD_SOCK_DIR}"

echo
echo 'Test the saslauthd connection.'
LDAP_USER='adam'
LDAP_PASS='password'
testsaslauthd -u "${LDAP_USER}" -p "${LDAP_PASS}" -f "${SASLAUTHD_SOCK_DIR}/mux"
