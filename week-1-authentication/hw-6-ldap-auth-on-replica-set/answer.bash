#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

m310_print_header 'Unzipping homework data.'
HW_SCRIPTS_DIR='m310-hw-1.6'
HW_VAGRANT_DIR='m310-vagrant-env'
HW_SHARED_DIR="${HW_VAGRANT_DIR}/shared"
rm -rfv "${HW_SCRIPTS_DIR}"
rm -rfv "${HW_VAGRANT_DIR}"
rm -rfv "${HW_SHARED_DIR}"
unzip ../chapter_1_authentication.fd60fc419fad.zip "${HW_SCRIPTS_DIR}/*"
unzip ../chapter_1_authentication.fd60fc419fad.zip "${HW_VAGRANT_DIR}/*"

m310_print_header 'Setting up Vagrant environment.'
HW_LDAP_CONFIG='ldapconfig.py'
HW_SETUP_SCRIPT='setup-hw-1.6.sh'
HW_SETUP_DB_SCRIPT='setup-database.bash'
HW_SETUP_INF_SCRIPT='setup-infrastructure.bash'
HW_VALIDATION_SCRIPT="validate-hw-1.6.sh"
mv -v "${HW_SCRIPTS_DIR}/" "${HW_SHARED_DIR}"
rm -rfv "${HW_SCRIPTS_DIR}"
cp -v "${HW_SETUP_DB_SCRIPT}" "${HW_SHARED_DIR}"
cp -v "${HW_SETUP_INF_SCRIPT}" "${HW_SHARED_DIR}"
chmod 755 -c "${HW_SHARED_DIR}/${HW_SETUP_SCRIPT}"
chmod 755 -c "${HW_SHARED_DIR}/${HW_SETUP_DB_SCRIPT}"
chmod 755 -c "${HW_SHARED_DIR}/${HW_SETUP_INF_SCRIPT}"
chmod 755 -c "${HW_SHARED_DIR}/${HW_VALIDATION_SCRIPT}"

m310_print_header 'Setting up VMs'
pushd "${HW_VAGRANT_DIR}"
vagrant plugin install vagrant-vbguest
vagrant up

m310_print_header 'Setting up LDAP environment.'
vagrant ssh infrastructure <<EOF
    bash -c "shared/${HW_SETUP_SCRIPT}"
EOF

m310_print_header 'Setting up custom LDAP environment.'
vagrant ssh infrastructure <<EOF
    bash -c "shared/${HW_SETUP_INF_SCRIPT}"
EOF

m310_print_header 'Setting up the MongoDB replica set.'
vagrant ssh database <<EOF
    bash -c "shared/${HW_SETUP_DB_SCRIPT}"
EOF

m310_print_header 'Changing Adams password.'
NEW_PASSWORD='webscale'
vagrant ssh infrastructure <<EOF
    python shared/ldapconfig.py passwd -u adam -op password -np "${NEW_PASSWORD}"
    sudo service saslauthd restart
EOF

m310_print_header 'Verifying Adams new password, should see a 1 on the output.'
vagrant ssh database <<EOF_SSH
    mongo --port="31160" <<EOF_MONGO
        use admin;
        db.auth({
            user: 'adam',
            pwd: "${NEW_PASSWORD}",
            });
EOF_MONGO
EOF_SSH


m310_print_header 'Validating environment.'
vagrant ssh database <<EOF
    bash -c "shared/${HW_VALIDATION_SCRIPT}"
EOF

m310_print_header 'Cleaning up VMs'
vagrant -f destroy
popd

m310_print_header 'Cleaning up Vagrant environment.'
rm -rfv "${HW_VAGRANT_DIR}"
