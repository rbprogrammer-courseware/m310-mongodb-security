#!/usr/bin/env bash

# FIXME this script isn't 100% complete, but it's mostly there to get the answer.

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

m310_print_header 'Unzipping homework data.'
VAGRANT_DIR='m310-vagrant-env'
HW_DATA_DIR='m310-hw-1.2'
rm -rf "${VAGRANT_DIR}"
unzip ../chapter_1_authentication.fd60fc419fad.zip "${VAGRANT_DIR}/*"
unzip ../chapter_1_authentication.fd60fc419fad.zip "${HW_DATA_DIR}/*"

m310_print_header 'Moving into Vagrant environment.'
pushd "${VAGRANT_DIR}"

m310_print_header 'Setting up VMs'
vagrant plugin install vagrant-vbguest
vagrant up

m310_print_header 'Setting up Mongo replica-set.'
HW_SETUP_SCRIPT_NAME='setup-hw-1.2.sh'
cp "../${HW_DATA_DIR}/${HW_SETUP_SCRIPT_NAME}" "shared/"
vagrant ssh database <<EOF
    bash -c shared/${HW_SETUP_SCRIPT_NAME}
    sleep 10s
EOF

m310_print_header 'Shutting down the current Mongo replica-set.'
sleep 10s
WORKING_DIR='/home/vagrant/M310-HW-1.2'
vagrant ssh database <<EOF_SSH
    mongod --shutdown --dbpath="${WORKING_DIR}/r0"
    mongod --shutdown --dbpath="${WORKING_DIR}/r1"
    mongod --shutdown --dbpath="${WORKING_DIR}/r2"
    killall mongod
EOF_SSH

m310_print_header 'Creating a common key-file for a new Mongo replica-set.'
KEYFILE='shared/mongod.key'
openssl rand -base64 755 >"${KEYFILE}"
chmod 400 "${KEYFILE}"

m310_print_header 'Bringing up the replica-set with the key-file.'
PORTS=(31120 31121 31122)
REPL_SET_NAME="TO_BE_SECURED"
LOG_NAME='mongo.log'
vagrant ssh database <<EOF_SSH
    for ((i=0; i < ${#PORTS[@]}; i++)) ; do
        mongod --fork \
            --dbpath="${WORKING_DIR}/r${i}" \
            --logpath="${WORKING_DIR}/r${i}/${LOG_NAME}.log" \
            --port=${PORTS[${i}]} \
            --replSet="${REPL_SET_NAME}" \
            --keyFile="/home/vagrant/${KEYFILE}"
        sleep 10s
    done
EOF_SSH

m310_print_header 'Adding a new user on the primary.'
vagrant ssh database <<EOF_SSH
    mongo --port "${PORTS[0]}" <<EOF_MONGO
        use admin
        db.createUser({user:'admin', pwd: 'webscale', roles: ['root']})
EOF_MONGO
EOF_SSH

m310_print_header 'Validating environment.'
HW_VALIDATION_SCRIPT_NAME='validate-hw-1.2.sh'
cp "../${HW_DATA_DIR}/${HW_VALIDATION_SCRIPT_NAME}" "shared/"
vagrant ssh database <<EOF_SSH
    bash -c shared/${HW_VALIDATION_SCRIPT_NAME}
EOF_SSH

m310_print_header 'Cleaning up VMs'
vagrant -f destroy <<EOF
y
y
EOF

m310_print_header 'Leaving Vagrant environment.'
popd

m310_print_header 'Cleaning temporary project files.'
rm -rfv "${VAGRANT_DIR}"
rm -rfv "${HW_DATA_DIR}"
